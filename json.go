package togo

import (
	"encoding/json"
	"fmt"
)

func FromJSON(name string, data []byte) (GoStruct, error) {
	var m map[string]interface{}
	if err := json.Unmarshal(data, &m); err != nil {
		return GoStruct{}, fmt.Errorf("could not unmarshal JSON: %v", err)
	}
	gs := ToGo(name, m)
	gs.format = jsonFormat
	return gs, nil
}
