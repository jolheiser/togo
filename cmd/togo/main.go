package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"go.jolheiser.com/togo"

	"github.com/huandu/xstrings"
)

var (
	nameFlag string
	jsonFlag bool
	tomlFlag bool
	yamlFlag bool
	fileFlag *os.File
	outFlag  = os.Stdout
)

func main() {
	flag.StringVar(&nameFlag, "name", "", "Struct name, defaults to T or filename (exported) if --file is set")
	flag.BoolVar(&jsonFlag, "json", false, "JSON-to-Go")
	flag.BoolVar(&tomlFlag, "toml", false, "TOML-to-Go")
	flag.BoolVar(&yamlFlag, "yaml", false, "YAML-to-Go")
	flag.Func("file", "File input", func(s string) (err error) {
		fileFlag, err = os.Open(s)
		return
	})
	flag.Func("out", "Output", func(s string) (err error) {
		outFlag, err = os.Create(s)
		return
	})
	flag.Parse()

	// Check exclusivity
	if (jsonFlag && tomlFlag) || (tomlFlag && yamlFlag) || (jsonFlag && yamlFlag) {
		fmt.Println("--json, --toml, and --yaml are exclusive to each other")
		return
	}

	if err := toGo(); err != nil {
		fmt.Println(err)
	}
}

func toGo() error {
	name := "T"
	var fn func(name string, data []byte) (togo.GoStruct, error)
	data := []byte(strings.Join(flag.Args(), " "))
	if fileFlag != nil {
		defer fileFlag.Close()
		contents, err := io.ReadAll(fileFlag)
		if err != nil {
			return err
		}
		data = contents
		ext := filepath.Ext(fileFlag.Name())
		switch ext {
		case ".json":
			fn = togo.FromJSON
		case ".toml":
			fn = togo.FromTOML
		case ".yaml":
			fn = togo.FromYAML
		}
		name = xstrings.ToCamelCase(strings.TrimSuffix(filepath.Base(fileFlag.Name()), ext))
	}

	// Flags take priority
	if nameFlag != "" {
		name = nameFlag
	}
	if jsonFlag {
		fn = togo.FromJSON
	}
	if tomlFlag {
		fn = togo.FromTOML
	}
	if yamlFlag {
		fn = togo.FromYAML
	}

	if fn == nil {
		return errors.New("no format specified or could not be inferred from file name")
	}

	gs, err := fn(name, data)
	if err != nil {
		return err
	}

	return gs.Fprint(outFlag, gs.DefaultTagOptions())
}
