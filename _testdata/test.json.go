package testdata

type J struct {
	ID      string  `json:"id" `
	Type    string  `json:"type" `
	Name    string  `json:"name" `
	Ppu     float64 `json:"ppu" `
	Amount  float64 `json:"amount" `
	Batters Batters `json:"batters" `
}

type Batters struct {
	Batter []Batter `json:"batter" `
}

type Batter struct {
	Type string `json:"type" `
}
