package testdata

type T struct {
	Integer int     `toml:"integer" `
	Float   float64 `toml:"float" `
	Owner   Owner   `toml:"owner" `
	Title   string  `toml:"title" `
}

type Owner struct {
	DOB          string `toml:"dob" `
	Organization string `toml:"organization" `
	Bio          string `toml:"Bio" `
}
