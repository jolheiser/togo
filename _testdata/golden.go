//+build ignored

package main

import (
	_ "embed"
	"fmt"
	"os"

	"go.jolheiser.com/togo"
)

var (
	//go:embed test.json
	testJSON []byte
	//go:embed test.toml
	testTOML []byte
	//go:embed test.yaml
	testYAML []byte
)

func main() {
	j, err := togo.FromJSON("J", testJSON)
	if err != nil {
		panic(err)
	}
	mustWrite(j, "json")

	t, err := togo.FromTOML("T", testTOML)
	if err != nil {
		panic(err)
	}
	mustWrite(t, "toml")

	y, err := togo.FromYAML("Y", testYAML)
	if err != nil {
		panic(err)
	}
	mustWrite(y, "yaml")
}

func mustWrite(g togo.GoStruct, format string) {
	fi, err := os.Create(fmt.Sprintf("test.%s.go", format))
	if err != nil {
		panic(err)
	}
	defer fi.Close()
	if _, err := fi.WriteString("package testdata\n"); err != nil {
		panic(err)
	}
	if err := g.Fprint(fi, g.DefaultTagOptions()); err != nil {
		panic(err)
	}
}
