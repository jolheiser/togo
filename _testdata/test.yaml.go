package testdata

type Y struct {
	Float    float64  `yaml:"float" `
	Eyes     string   `yaml:"eyes" `
	Beard    bool     `yaml:"beard" `
	Hacker   bool     `yaml:"Hacker" `
	Name     string   `yaml:"name" `
	Hobbies  []string `yaml:"hobbies" `
	Clothing Clothing `yaml:"clothing" `
	Age      int      `yaml:"age" `
}

type Clothing struct {
	Jacket   string `yaml:"jacket" `
	Trousers string `yaml:"trousers" `
	Pants    Pants  `yaml:"pants" `
}

type Pants struct {
	Size string `yaml:"size" `
}
