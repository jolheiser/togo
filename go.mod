module go.jolheiser.com/togo

go 1.16

require (
	github.com/huandu/xstrings v1.3.2
	github.com/pelletier/go-toml v1.9.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
