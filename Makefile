GO ?= go

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: test
test:
	$(GO) test -race ./...

.PHONY: golden
golden:
	cd _testdata && $(GO) run golden.go
	$(GO) fmt _testdata/test.json.go _testdata/test.toml.go _testdata/test.yaml.go
