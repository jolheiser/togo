package togo

import (
	"fmt"

	"gopkg.in/yaml.v3"
)

func FromYAML(name string, data []byte) (GoStruct, error) {
	var m map[string]interface{}
	if err := yaml.Unmarshal(data, &m); err != nil {
		return GoStruct{}, fmt.Errorf("could not unmarshal YAML: %v", err)
	}
	gs := ToGo(name, m)
	gs.format = yamlFormat
	return gs, nil
}
