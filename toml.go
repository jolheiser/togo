package togo

import (
	"fmt"

	"github.com/pelletier/go-toml"
)

func FromTOML(name string, data []byte) (GoStruct, error) {
	var m map[string]interface{}
	if err := toml.Unmarshal(data, &m); err != nil {
		return GoStruct{}, fmt.Errorf("could not unmarshal TOML: %v", err)
	}
	gs := ToGo(name, m)
	gs.format = tomlFormat
	return gs, nil
}
